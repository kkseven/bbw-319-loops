package ch.bbw.m319.loops;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class LoopHardTest {

	/**
	 *
	 * Lektüre: https://www.sofatutor.ch/mathematik/zahlen-rechnen-und-groessen/teilbarkeit-und-mengen/primzahlen <br>
	 * Lektüre: http://www.tutego.de/blog/javainsel/2019/04/der-restwert-operator/ <br>
	 *
	 * Eine Primzahl ist eine natürliche Zahl mit genau zwei verschiedenen natürlichen Zahlen als Teiler, nämlich der
	 * Zahl 1 und sich selbst. Die Zahl 1 hat nur einen Teiler (sich selbst) und zählt damit nicht als Primzahl.
	 *
	 * @param number the number we want to check
	 * @return true if the number is a prime number
	 */
	boolean isPrimeNumber(int number) {
		// TODO: implement a for-loop here
		return false;
	}

	@Test
	void isPrimeNumber_test() {
		// DO NOT EDIT THIS METHOD, edit the isPrimeNumber(int) above instead
		assertTrue(isPrimeNumber(1));
		assertTrue(isPrimeNumber(3));
		assertTrue(isPrimeNumber(13));
		assertTrue(isPrimeNumber(7393));
		assertTrue(isPrimeNumber(2));

		assertFalse(isPrimeNumber(9));
		assertFalse(isPrimeNumber(666));
		assertFalse(isPrimeNumber(877 * 313));
	}

	/**
	 * Zähle in einem String alle Wörter welche mit "y" oder mit "z" enden. Also y in "heavy" zählt und auch das z in "fez",
	 * aber das y in "yellow" gilt nicht. Wir sagen dass ein y oder x am Ende des Wortes ist, wenn darauf kein alphanumerisches
	 * Zeichen folgt. (Anm: Character.isLetter(char) tested ob ein char alphanumerisch ist)
	 *
	 * countYZ("fez day") → 2
	 */
	int countYZ(String str) {
		// TODO: implement me
		return 0;
	}

	@Test
	void countYZ_test() {
		// DO NOT EDIT THIS METHOD, edit the countYZ(String) above instead
		assertEquals(0, countYZ("nope"));
		assertEquals(0, countYZ("yellow"));
		assertEquals(0, countYZ("so yellow"));
		assertEquals(1, countYZ("what a day"));
		assertEquals(1, countYZ("i can haz cheezburger"));
		assertEquals(2, countYZ("fez day"));
		assertEquals(2, countYZ("day fez"));
		assertEquals(2, countYZ("day fyyyz"));
		assertEquals(3, countYZ("day y yes yolo yay"));
	}

	/**
	 * Gib die maximale Anzahl Zeichenrepetitionen in einem String zurück.
	 *
	 * @param str in which to search
	 * @return maximum repetition block size
	 */
	int maxBlock(String str) {
		// TODO: implement me
		return 0;
	}

	@Test
	void maxBlock_test() {
		// DO NOT EDIT THIS METHOD, edit the maxBlock(String) above instead
		assertEquals(2, maxBlock("hoopla"));
		assertEquals(3, maxBlock("abbCCCddBBBxx"));
		assertEquals(9, maxBlock("aaaaaaaaa"));
		assertEquals(0, maxBlock(""));
	}

	/**
	 * Such ein Teilstring, welcher gleich ist wie der Anfang des Strings aber gespiegelt am Ende des Strings steht.
	 * Also 0 oder mehr Zeichen am Anfang, so dass dieselben Zeichen in umgekehrter Reihenfolge am Ende wider vorkommen.
	 * Zum Beispiel steht im String "abXYZba", die Zeichenfolge "ab" am Anfang und gespiegelt wider am Schluss.
	 *
	 * @param str in which to search
	 * @return maximum start-string mirrored at the end
	 */
	String mirrorEnds(String str) {
		// TODO: implement me
		return str;
	}

	@Test
	void mirrorEnds_test() {
		// DO NOT EDIT THIS METHOD, edit the maxBlock(String) above instead
		assertEquals("ab", mirrorEnds("abXYZba"));
		assertEquals("a", mirrorEnds("abca"));
		assertEquals("", mirrorEnds("abc"));
		assertEquals("HuHuH", mirrorEnds("HuHuH"));
	}
}
