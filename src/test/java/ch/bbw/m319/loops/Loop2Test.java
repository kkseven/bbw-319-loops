package ch.bbw.m319.loops;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class Loop2Test {

	/**
	 * Lektüre: http://www.mathematische-basteleien.de/dreieckszahlen.htm <br>
	 *
	 * Eine Dreieckszahl ist eine Zahl welche sich aus der Summe
	 * von 1 und aller darauffolgenden natürlichen Zahlen (ganze
	 * Zahlen) bis zu einer Obergrenze "n" ergibt.
	 * zB.: d3 = 1 + 2 + 3 = 6 (ist die 3-te Dreieckszahl)
	 *
	 * @param n the index of the triangular number we want
	 * @return the n-th triangular number
	 */
	int triangularNumber(int n) {
		int sum = 0;
		// TODO: implement a for-loop here
		return 1;
	}

	@Test
	void triangularNumber_test() {
		// DO NOT EDIT THIS METHOD, edit the triangularNumber(int) above instead
		assertEquals(1, triangularNumber(1));
		assertEquals(55, triangularNumber(10));
		assertEquals(0, triangularNumber(0));
	}

	/**
	 * Eine methode, weche einen String repetiert.
	 * zB. "abc" drei mal wird "abcabcabc".
	 *
	 * @param str    the string to multiply
	 * @param amount how many times to repeat the string
	 * @return a new string repeated n times
	 */
	String repeatString(String str, int amount) {
		// TODO: implement a for-loop here
		return str;
	}

	@Test
	void repeatString_test() {
		// DO NOT EDIT THIS METHOD, edit the repeatString(String,int) method above instead
		assertEquals("x", repeatString("x", 1));
		assertEquals("***", repeatString("*", 3));
		assertEquals("##########", repeatString("#", 10));
		assertEquals("abcabc", repeatString("abc", 2));
		assertEquals("", repeatString("#", 0));
	}

	/**
	 * Lektüre: wie man über die Zeichen eines Strings iteriert https://stackoverflow.com/a/196834
	 * <p>
	 * Zähl die Anzahl "x" in einem gegebenen String. also "xxx" -> 3, "axbxcx" -> 3
	 *
	 * @param searchString string in which to count the amount of x
	 * @return the number of x in that string
	 */
	int countX(String searchString) {
		int sum = 0;
		// TODO: implement a for-loop here
		return sum;
	}

	@Test
	void countX_test() {
		// DO NOT EDIT THIS METHOD, edit the countX(String) above instead
		assertEquals(0, countX("no iksch in here"));
		assertEquals(0, countX(""));
		assertEquals(1, countX("        x"));
		assertEquals(1, countX("x        "));
		assertEquals(4, countX("  x xx x "));
	}

	/**
	 * TODO: schreib eine Beschreibung, was diese Methode macht
	 *
	 * @param a
	 * @param b
	 * @return
	 */
	final int doSomething(int a, int b) {
		// DO NOT EDIT THIS METHOD, edit the do_something_test(String) below instead
		int x = a;
		for (int i = 1; i < b; i++) {
			x += a;
		}
		return x;
	}

	@Test
	void doSomething_test() {
		// TODO: implementiere 3 sinnvolle Tests für diese Methode
	}

	/**
	 * Lektüre: http://www.abi-mathe.de/buch/grundlagen/fibonacci-folge/ <br>
	 * <p>
	 * die erwartete Reihenfolge ist 1,1,2,3,4,5,13,21,... also für n=1 erwarten wir als Rückgabewert 1, für n=7 erwarten wir 13,
	 * usw
	 *
	 * @param n the index of the Fibonacci sequence (at least 3)
	 * @return the n-th Fibonacci number
	 */
	long fibonacciForLoop(int n) {
		if (n < 2) {
			throw new IllegalArgumentException("we don't support small n");
		}
		long last = 1;
		long sum = 1;
		// TODO: implement a for-loop here
		return sum;
	}

	@Test
	void fibonacciForLoop_test() {
		// DO NOT EDIT THIS METHOD, edit the fibonacciForLoop(int) above instead
		assertEquals(2L, fibonacciForLoop(3));
		assertEquals(8L, fibonacciForLoop(6));
		assertEquals(832040L, fibonacciForLoop(30));
		assertEquals(63245986L, fibonacciForLoop(39));
	}
}
