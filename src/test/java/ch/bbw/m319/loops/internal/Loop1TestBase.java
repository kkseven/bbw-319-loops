package ch.bbw.m319.loops.internal;

import org.junit.jupiter.api.Assertions;

import java.util.Objects;

public abstract class Loop1TestBase {
	// DO NOT EDIT THIS CODE
	protected static final Object CHANGEME = "change me!";

	protected void fail(String msg) {
		org.junit.jupiter.api.Assertions.fail(msg);
	}

	protected void assertEquals(Object expected, Object actual) {
		if (CHANGEME == expected || CHANGEME == actual) {
			fail("you have to replace the CHANGEME variable");
		}
		// if we'd use assertEqual() we'd spoil the solution :)
		Assertions.assertTrue(Objects.equals(expected, actual), "sorry, but two variables in assertEquals(a,b) don't match");
		if (expected instanceof String && expected == actual) {
			fail("using the same variable twice is kinda cheating");
		}
	}
	// DO NOT EDIT THIS CODE
}
