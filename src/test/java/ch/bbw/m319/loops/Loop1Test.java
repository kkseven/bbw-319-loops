package ch.bbw.m319.loops;

import ch.bbw.m319.loops.internal.Loop1TestBase;
import org.junit.jupiter.api.Test;

/**
 * Lektüre: <a href="https://www.java-tutorial.org/schleifen.html">https://www.java-tutorial.org/schleifen.html</a>.
 * Ziel ist es durch kleine Änderungen jeden Test "grün" zu machen und dabei jeweils ein kleines Java-Konzept zu lernen.
 */
@SuppressWarnings("StringConcatenationInLoop")
class Loop1Test extends Loop1TestBase {

	@Test
	void just_delete_one_line_to_advance() {
		fail("delete this whole line to advance");
	}

	@Test
	void uncomment_one_line_to_advance() {
		var x = 0;
		// x = 2
		assertEquals(2, x);
	}

	@Test
	void comment_out_a_single_line() {
		var x = 0;
		x = x + 1;
		x = x + 2;
		x = x + 3;
		x = x + 4;
		x = x + 5;
		assertEquals(11, x);
	}

	@Test
	void comment_out_a_single_line_2() {
		var x = 0;
		x++;
		x += 2;
		x += x;
		x -= 4;
		x *= 2;
		assertEquals(0, x);
	}

	@Test
	void a_basic_for_loop() {
		// In diesem Test ist ausschliesslich das CHANGEME durch eine Zahl zu ersetzen.
		var counter = 0;
		for (int i = 0; i < 3; i++) {
			counter = counter + 1;
		}
		assertEquals(counter, CHANGEME);
	}

	@Test
	void a_basic_for_loop_looks_familiar() {
		var counter = 0;
		for (var i = 0; i < 5; i++) {
			counter++; // yes, just a shortform for x = x + 1
		}
		assertEquals(counter, CHANGEME);
	}

	@Test
	void whats_a_string() {
		// In diesem Test ist ausschliesslich das CHANGEME durch einen "String" zu ersetzen.
		var aString = "some text";
		aString += " where ";
		aString += "we append!";
		assertEquals(aString, CHANGEME);
	}

	@Test
	void a_string_in_a_loop() {
		var s = "";
		for (var i = 0; i < 5; i++) {
			s += i + " ";
		}
		assertEquals(s, CHANGEME);
	}

	@Test
	void a_string_in_a_loop_again() {
		var s = "";
		for (int i = -5; i < 1; i++) {
			s += i + " ";
		}
		assertEquals(s, CHANGEME);
	}

	@Test
	void a_string_in_a_loop_minus() {
		var s = "";
		for (var i = 5; i > 0; i--) {
			s += i + " ";
		}
		assertEquals(s, CHANGEME);
	}

	@Test
	void a_string_in_a_loop_4() {
		var s = "";
		for (var i = 0; i < 11; i += 2) {
			s += i + " ";
		}
		assertEquals(s, CHANGEME);
	}

	@Test
	void a_string_in_a_loop_5() {
		var s = "";
		for (var i = 1; i <= 16; i *= 2) {
			s += i + " ";
		}
		assertEquals(s, CHANGEME);
	}

	@Test
	void a_loop_with_two_variables() {
		var s = "";
		for (int i = 0, j = 10; i < 5 && j > 5; i++, j--) {
			s += i + " " + j + " ";
		}
		assertEquals(s, CHANGEME);
	}

	@Test
	void nested_loops() {
		var s = "";
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				s += i + "" + j + " ";
			}
			s += ", ";
		}
		assertEquals(s, CHANGEME);
	}

	@Test
	void while_loop() {
		int result = 0;
		while (result < 3) {
			result++;
		}
		assertEquals(result, CHANGEME);
	}

	@Test
	void while_with_break() {
		int result = 0;
		while (true) {
			result++;
			if (result > 3) {
				break;
			}
		}
		assertEquals(result, CHANGEME);
	}
}
