# Loops

Ziel:
- Wir nutzen `for`-loops als imperative Kontrollstruktur.
  ([tutorial](https://www.java-tutorial.org/schleifen.html))
- Wir arbeiten mit Ansätzen von Test-Driven-Design (TDD).
- Wir erweitern / korrigieren bereits existierenden Code.

Aufgabe:
- Alle Aufgaben befinden sich in `src/test/java/.../*Test.java`.
- Eine Aufgabe ist generell erfüllt, sobald der Test "grün" ist.
- Aufgaben sind generell chronologisch zu lösen, überspringen ist aber ok.
- zu schwer oder zu einfach? Alternativübungen auf https://codingbat.com/java
